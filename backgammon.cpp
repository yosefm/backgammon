#include <fstream>
#include <iostream>
#include <iomanip>
#include <cstdlib>

#include <map>
#include <unordered_map>
#include <set>

#include <limits>
#include <algorithm>
#include <memory>

#include "bgboard.h"
#include "playingbot.h"
#include "bggame.h"

using namespace Purim;

const int maxVisibleCheckers = 5;
const std::map<Direction, std::string> checkerFace = {
    {Direction::CW, "    O"}, {Direction::CCW, "    X"}, {Direction::None, "     "}
};

void DisplayBoard(const PointsOccupancy state) {
    const std::string emptyPoint = "     ";
    const std::string emptyBar = " (  )";
    const short pointWidth = emptyPoint.length();
    
    // Upper Labels:
    for (short pos = 13; pos < 19; pos++)
        std::cout << std::setw(pointWidth) << pos;
    std::cout << "  Bar";
    for (short pos = 19; pos < 25; pos++)
        std::cout << std::setw(pointWidth) << pos;
    std::cout << "  Out" << std::endl;
    
    // Upper checkers:
    for (int line = 0; line < maxVisibleCheckers - 1; line++) {
        for (short pos = 13; pos < 25; pos++) {
            if (state[pos].first > line)
                std::cout << checkerFace.at(state[pos].second);
            else
                std::cout << emptyPoint;
            
            // How many on the bar?
            if (pos == 18) {
                if (line == 2) 
                    std::cout << " (" << state[25].first << "X)"; // 5 chars.
                else
                    std::cout << emptyBar; 
            }
        }
        std::cout << std::endl;
    }
    // Last line shows a number if more checkers than showable in point.
    for (short pos = 13; pos < 25; pos++) {
        if (state[pos].first == maxVisibleCheckers)
            std::cout << checkerFace.at(state[pos].second);
        else if (state[pos].first > maxVisibleCheckers)
            std::cout << std::setw(pointWidth) << state[pos].first;
        else
            std::cout << emptyPoint;
        
        if (pos == 18) std::cout << emptyPoint; 
    }
    std::cout << std::endl;
    
    // Vertical space between up and down sides:
    std::cout << std::endl << std::endl;
    
    // Overflow line for bottom checkers:
    for (short pos = 12; pos > 0; pos--) {
        if (state[pos].first == maxVisibleCheckers)
            std::cout << checkerFace.at(state[pos].second);
        else if (state[pos].first > maxVisibleCheckers)
            std::cout << std::setw(pointWidth) << state[pos].first;
        else
            std::cout << emptyPoint;
        if (pos == 7) std::cout << emptyPoint; 
    }
    std::cout << std::endl;
    
    // Bottom checkers:
    for (int line = maxVisibleCheckers - 2; line >= 0; line--) {
        for (short pos = 12; pos > 0; pos--) {
            if (state[pos].first > line)
                std::cout << checkerFace.at(state[pos].second);
            else
                std::cout << emptyPoint;
            if (pos == 7) {
                if (line == 2) 
                    std::cout << " (" << state[0].first << "O)"; // 5 chars.
                else
                    std::cout << emptyBar; 
            }
        }
        std::cout << std::endl;
    }
    
    // Lower Labels:
    for (short pos = 12; pos > 6; pos--)
        std::cout << std::setw(pointWidth) << pos;
    std::cout << "  Bar";
    for (short pos = 6; pos > 0; pos--)
        std::cout << std::setw(pointWidth) << pos;
    std::cout << "  Out" << std::endl;
    
    // Clearance.
    std::cout << std::endl;
}

// Very primitive, no input checking at all. 
bool LoadGame(std::string filename, std::vector<PointsOccupancy::Point>& loadedVec) {
    std::ifstream fs {filename};
    
    char face;
    unsigned short checkerCount;
    for (int point = 0; point < 26; ++point) {
        fs >> face >> checkerCount;
        
        switch (face) {
            case 'X':
                loadedVec.push_back(std::make_pair(checkerCount, Direction::CCW));
                break;
            case 'O':
                loadedVec.push_back(std::make_pair(checkerCount, Direction::CW));
                break;
            case '-':
                loadedVec.push_back(std::make_pair(checkerCount, Direction::None));
                break;
            default:
                throw std::runtime_error("file format is wrong on so many levels.");
        }
    }
}

void waitForKey() {
    std::cout << "Press Enter for next move.";
    char whatever = std::cin.get();
    std::cout << std::endl;
}

int main(int argc, char* argv[]) {
    BackGammonGame board;
    if (argc == 2) {
        std::vector<PointsOccupancy::Point> loaded;
        LoadGame(argv[1], loaded);
        board.RestartGame(loaded);
    }
    
    std::unordered_map<Direction, std::unique_ptr<DecisionMaker>> participants;
    participants[Direction::CW] = std::unique_ptr<DecisionMaker>(new HumanPlayer);
    participants[Direction::CCW] = std::unique_ptr<DecisionMaker>(new SheshBeshBot); 

    DisplayBoard(board.currentState());
    std::cout << "Turn: " << checkerFace.at(board.NextPlayer()) << std::endl;
        
    waitForKey();
    
    BackGammonGame::GameState gameState = BackGammonGame::GameState::Ongoing;
    while (gameState == BackGammonGame::GameState::Ongoing) {
        auto roll = board.Roll();
        std::cout << "Rolled " << roll.first << ", " << roll.second << std::endl;
                
        // Generate and show plays list:
        std::set<Play> availablePlays = board.AvailablePlaysForNextPlayer(roll);
        if (availablePlays.size() < 1) {
            std::cout << "Sorry, no plays are available for you in this roll." << std::endl;
            board.ExecutePlay({});
            continue;
        }
        
        unsigned int playChoice = std::numeric_limits<unsigned int>::max();
        while (playChoice > availablePlays.size()) {
            playChoice = participants[board.NextPlayer()]->SelectPlay(availablePlays);
        }
        
        // execute selected move:
        auto playIt = availablePlays.begin();
        std::advance(playIt, playChoice);
        gameState = board.ExecutePlay(*playIt);
        DisplayBoard(board.currentState());
        
        std::cout << "Turn: " << checkerFace.at(board.NextPlayer()) << std::endl;
        
        //waitForKey();
    }
    
    return EXIT_SUCCESS;
}
