/* Manage a BackGammon board.
 * 
 * References:
 * [1] http://www.bkgm.com/articles/Berliner/BKG-AProgramThatPlaysBackgammon/
*/

#pragma once

#include <array>
#include <vector>
#include <random>
#include <functional>
#include <initializer_list>

namespace Purim 
{
    using Move = std::pair<short, short>; // (point, distance)
    using Play = std::vector<Move>;

    // Players are characterized by their movement direction.
    // Coloring is someone else's problem.
    enum class Direction : short { CW, CCW, None };
    inline Direction operator!(Direction pl) {
        switch (pl) {
            case Direction::CW:
                return Direction::CCW;
            case Direction::CCW:
                return Direction::CW;
            default:
                return Direction::None;
        }
    }
    
    // Deals with numbering the board for a player so that 
    // moves are in the right direction, home/bar positions
    // are found by player direction etc.
    class PlayerBoard {
        unsigned short m_start, m_end, m_home, m_bar;
        short m_advance;
        std::function<bool(unsigned short, unsigned short)> backward_equal;

        static PlayerBoard CW;
        static PlayerBoard CCW;
        
    public:
        class iterator {
            short m_advance;
            friend class PlayerBoard;
            
        public:
            unsigned short operator *() const { return i_; }
            const iterator &operator ++() { i_ += m_advance; return *this; }
            iterator operator ++(int) { iterator copy(*this); ++i_; return copy; }

            bool operator ==(const iterator &other) const { return i_ == other.i_; }
            bool operator !=(const iterator &other) const { return i_ != other.i_; }
            
        protected:
            iterator(unsigned short start, short advance) : m_advance{advance}, i_ (start) { }
            
        private:
            unsigned short i_;
        };
        // Iterate the global numbering in the direction of player movement (i.e 24->1 for CCW, 1->24 for CW).
        iterator begin() { return iterator(m_start, m_advance); }
        iterator end() { return iterator(m_end + m_advance, m_advance); }
        
        PlayerBoard(Direction dir)
        {
            if (dir == Direction::CW) {
                m_start = 1;
                m_end = 24;
                m_advance = 1;
                m_home = 19;
                m_bar = 0;
                backward_equal = std::less_equal<short>{};
            } else {
                m_start = 24;
                m_end = 1;
                m_advance = -1;
                m_home = 6;
                m_bar = 25;
                backward_equal = std::greater_equal<short>{};
            }
        }
        
        bool IsHome(short point) { return backward_equal(m_home, point) && backward_equal(point, m_end); }
        bool CarriedOff(short point) { return !backward_equal(point, m_end); }
        
        unsigned short Bar() { return m_bar; }
        unsigned short EndPoint() { return m_end; }
        
        short AdvanceFrom(unsigned short from, unsigned short distance) {
            return from + distance*m_advance;
        }
        short Advance(unsigned short distance) {
            return m_start + distance*m_advance;
        }
        
        // Get the singleton PlayerBoard instance for a player going in the
        // given direction.
        static PlayerBoard GetFor(Direction pl) {
            return (pl == Direction::CW) ? CW : CCW;
        }
    };
    
    // 0 and 25 are bar for CCW and CW, respectively).
    class PointsOccupancy {
        std::vector<std::pair<short, Direction>> m_points;
        
        enum class LegalMovesSet { OnBar, CanBearOff, Normal};
        
        // Internally, CW is first and CCW is second.
        // requires casting but less wasteful than map.
        std::array<LegalMovesSet, 2> m_playerMoveSets;
        std::array<short, 2> m_playerRears;
        void UpdateLegalSet();
        
    public:
        using Point = std::pair<short, Direction>;
        PointsOccupancy() {}
        PointsOccupancy(std::vector<Point> init_pos);
        PointsOccupancy(std::initializer_list<Point> init_pos) : PointsOccupancy{std::vector<Point>(init_pos)} {}  
        
        PointsOccupancy& operator=(const PointsOccupancy&) = default;
        Point operator[](unsigned short which) const { return m_points[which]; }
        bool operator==(const PointsOccupancy& other) const { return m_points == other.m_points; }

        bool IsLegal(short point, short distance) const;
        short FindRear(Direction pl) const;
        PointsOccupancy Move(short point, short distance) const;
    };

}; // namespace.
