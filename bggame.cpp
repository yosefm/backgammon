#include <algorithm>

#include "bggame.h"
#include "playingbot.h"

#include <iostream>

namespace Purim {
    
    void BackGammonGame::RestartGame(std::vector<PointsOccupancy::Point> initialState) {
        if (initialState.size() == 26)
            m_positions = initialState;
        else
            m_positions = startPositions;
        
        //m_positions = carryOffPositions;
        //m_positions = blockedEntryPositions;
    }
    
    // Just for testing.
    BackGammonGame::GameState BackGammonGame::ExecutePlay() {
        m_dice = std::make_pair(m_diceRV(m_rndEng), m_diceRV(m_rndEng));
        
        // Here goes actual move.
        // Just a hard-coded attempt to check moving:
        auto ppos = PlayerBoard::GetFor(m_nextMover);
        short lastPos = m_positions.FindRear(m_nextMover);
        if (m_positions.IsLegal(lastPos, m_dice.first)) {
            m_positions = m_positions.Move(lastPos, m_dice.first);
        }
        
        m_nextMover = !m_nextMover;
        return GameState::Ongoing;
    }
    
    // Generate and show plays list:
    std::set<Play> BackGammonGame::AvailablePlaysForNextPlayer(std::pair<short, short> roll) {
        std::set<Play> availablePlays;
        std::vector<short> playLens; // not necessarily same order as set.
        
        std::vector<std::vector<short>> allowedMoves;
        allowedMoves.push_back({roll.first, roll.second});
        if (roll.first == roll.second) {
            allowedMoves[0].push_back(roll.first);
            allowedMoves[0].push_back(roll.first);
            allowedMoves.push_back({});
        } else {
            allowedMoves.push_back({roll.second, roll.first});
        }
        
        // Get all moves without repeating dice switch:
        for (auto& allowed : allowedMoves) {
            LegalPlaysIterator legalPlays(
                const_cast<PointsOccupancy&>(currentState()), NextPlayer(), allowed);
            LegalPlaysIterator playsEnd(const_cast<PointsOccupancy&>(currentState()), NextPlayer(), {});
            
            while (legalPlays != playsEnd)
            {   
                Play sortedPlay = *legalPlays;
                if (NextPlayer() == Direction::CW)
                    std::sort(sortedPlay.begin(), sortedPlay.end());
                else
                    std::sort(sortedPlay.rbegin(), sortedPlay.rend());
                
                availablePlays.insert(sortedPlay);
                playLens.push_back(sortedPlay.size());
                legalPlays++;
            }
        }
        
        if (availablePlays.size() == 0)
            return availablePlays;
        
        // Remove plays shorter than the longest possible play, since you have
        // to make every move you can make.
        unsigned short longestPlay = *std::max_element(playLens.begin(), playLens.end());
        auto it = availablePlays.begin();
        while (it != availablePlays.end()) {
            if (it->size() < longestPlay)
                availablePlays.erase(*it);
            it++;
        }
        
        return availablePlays;
    }
    
    BackGammonGame::GameState BackGammonGame::ExecutePlay(Play thisPlay) {
        for (auto& move : thisPlay) {
            if (m_positions.IsLegal(move.first, move.second)) {
                m_positions = m_positions.Move(move.first, move.second);
            } else { // End game, for now.
                return GameState::Error;
            }
        }
        
        // Check victory:
        short lastPoint = m_positions.FindRear(m_nextMover);
        PlayerBoard ppos = PlayerBoard::GetFor(m_nextMover);
        
        if (lastPoint == ppos.AdvanceFrom(ppos.EndPoint(), 1)) {
            short opPoint = m_positions.FindRear(!m_nextMover);
            if (ppos.IsHome(opPoint))
                return GameState::BackGammon;
            
            PlayerBoard opos = PlayerBoard::GetFor(!m_nextMover);
            if (opos.IsHome(opPoint)) // and started taking out, but never mind right now.
                return GameState::Won;
            
            return GameState::Gammon;
        }
        m_nextMover = !m_nextMover;
        return GameState::Ongoing;
    }
}
