#include "playingbot.h"
#include <iostream>

namespace Purim {
    LegalMovesIterator& LegalMovesIterator::operator++() {
        while (m_point < 25) {
            // Note that in the beginning m_point is -1 and the result is the bar,
            // which is before the start.
            short real_point = m_playerDesc.Advance(m_point);
            m_point++;
            
            if (m_baseline[real_point].second == m_player && m_baseline.IsLegal(real_point, m_die)) 
            {
                m_current = real_point;
                return *this;
            }
        }
        
        // Exceeded the end. Place in end position.
        m_point = -1;
        m_die = 0;
        return *this;
    }
    
    bool LegalMovesIterator::operator==(const LegalMovesIterator& other) {
        return (m_player == other.m_player) && (m_point == other.m_point) && (m_die == other.m_die) 
            && (m_baseline == other.m_baseline);
    }
    
    LegalPlaysIterator& LegalPlaysIterator::operator++() {
        // check if this is the 1st run or a return after a recorded play.
        short level = m_levelSeqs.size();
        if (level > 0) {
            auto& seq = m_levelSeqs.back();
            if (seq.begin() == seq.end()) {
                // Last play recorded because next dice had no moves.
                if (level == 1) {
                    // Exceeded the end. Place in end position.
                    m_diceComb = {};
                    return *this;
                }
                m_levelSeqs.pop_back();
                level--;
            } 
            
            m_current.pop_back();
            m_boardStack.pop_back();
            m_levelSeqs.back().begin()++;
            level--;
        } 
        else {
            // 1st run.
            m_levelSeqs.push_back(
                LegalMovesSequence(m_boardStack[0], m_player, m_diceComb[0]));
        }
        
        while (true) {
            // Check stop/yield conditions.
            auto& seq = m_levelSeqs.back();
            auto& it = seq.begin(); // points to candidate after last used move.
            
            if (it == seq.end()) {
                // If we reached the end we generate a play, even though it can be shorter
                // than what's possible to play in a different branch of the plays tree.
                // We leave it to the game code to filter out plays shorter than max possivle.
                return *this;
            }
            
            m_current.push_back(Move{*it, m_diceComb[level]});
            m_boardStack.emplace_back(m_boardStack.back().Move(*it, m_diceComb[level]));
            
            //std::cout << level << '\n';
            if (level == m_diceComb.size() - 1)
                return *this;
            
            level++;
            // When descending to next level, start a new moves iterator.
            if (m_levelSeqs.size() <= level) {
                m_levelSeqs.push_back(
                    LegalMovesSequence(m_boardStack[level], m_player, m_diceComb[level]));
            }
        }
        
        // Exceeded the end. Place in end position.
        m_diceComb = {};
    }
    
    bool LegalPlaysIterator::operator==(const LegalPlaysIterator& other) {
        // End position:
        if ((m_diceComb.size() == 0)  && (other.m_diceComb.size() == 0)) return true;
        
        return (m_player == other.m_player) && (m_diceComb == other.m_diceComb) 
            && ((other.m_diceComb.size() == 0) || (m_boardStack.back() == other.m_boardStack.back()));
    }
    
    unsigned int HumanPlayer::SelectPlay(std::set<Play> availablePlays) {
        unsigned int numPlays = 0;
        
        // List moves in set storage order:        
        std::cout << std::endl << "Legal plays: " << std::endl;
        for (auto play : availablePlays) {
            std::cout << "#" << numPlays << ": ";
            
            for (auto move : play) {
                std::cout << move.first << "/" << move.second << " " ;
            }
            std::cout << std::endl;
            numPlays++;
        }
        
        unsigned int playChoice = std::numeric_limits<unsigned int>::max();
        while (playChoice > availablePlays.size()) {
            std::cout << "Your choice? ";
            std::cin >> playChoice;
        }
        
        return playChoice;
    }
    
    unsigned int SheshBeshBot::SelectPlay(std::set<Play> availablePlays) {
        unsigned int playChoice = std::uniform_int_distribution<>(0, availablePlays.size() - 1)(rndEng);
        std::cout << "Choosing play " << playChoice << std::endl;
        return playChoice;
    }
}
