#include "bgboard.h"
#include <sstream>

// Debug:
#include <iostream>

namespace Purim 
{    
    PlayerBoard PlayerBoard::CW {Direction::CW};
    PlayerBoard PlayerBoard::CCW {Direction::CCW};
    
    PointsOccupancy::PointsOccupancy(std::vector<Point> init_pos) : 
        m_points(init_pos)
    {
        UpdateLegalSet();
    }
    
    void PointsOccupancy::UpdateLegalSet() {
        for (Direction pl : {Direction::CW, Direction::CCW}) {
            auto ppos = PlayerBoard::GetFor(pl);
        
            if (m_points[ppos.Bar()].second == pl)
                m_playerMoveSets[(short)pl] = LegalMovesSet::OnBar;
            else {
                short rear = FindRear(pl);
                m_playerRears[(short)pl] = rear;
                
                if (ppos.IsHome(rear))
                    m_playerMoveSets[(short)pl] = LegalMovesSet::CanBearOff;
                else
                    m_playerMoveSets[(short)pl] = LegalMovesSet::Normal;
            }
        }
    }
    
    short PointsOccupancy::FindRear(Direction pl) const {
        auto ppos = PlayerBoard::GetFor(pl);
        
        for (short scanPos : ppos)
            if (m_points[scanPos].second == pl) return scanPos;
        
        // Won:
        return ppos.AdvanceFrom(ppos.EndPoint(), 1);
    }
    
    bool PointsOccupancy::IsLegal(short point, short distance) const {
        Direction pl = m_points[point].second;
        if (pl == Direction::None)
            return false;
        
        auto ppos = PlayerBoard::GetFor(pl);
        if (ppos.CarriedOff(point))
            return false;
        
        short targetIndex = ppos.AdvanceFrom(point, distance);
        
        if (m_playerMoveSets[(short)pl] == LegalMovesSet::OnBar && point != ppos.Bar()) 
            return false;
        
        if (ppos.CarriedOff(targetIndex)) {
            if  (m_playerMoveSets[(short)pl] != LegalMovesSet::CanBearOff)
                return false;
            targetIndex = ppos.AdvanceFrom(ppos.EndPoint(), 1);
        }
        auto target = m_points[targetIndex];
        return (target.second == pl || target.second == Direction::None || target.first < 2);
    }
    
    PointsOccupancy PointsOccupancy::Move(short point, short distance) const {
        if (!IsLegal(point, distance)) {
            std::stringstream msg;
            msg << "Attempted illegal move: " << point << " -> " << distance << std::endl;
            throw std::runtime_error(msg.str());
        }
        
        PointsOccupancy ret(*this);
        
        // remove from start point.
        auto& src = ret.m_points[point];
        Direction pl = src.second, opp = !pl;
        src.first--;
        if (src.first == 0)
            src.second = Direction::None;
        
        // Clip target to board.
        auto ppos = PlayerBoard::GetFor(pl);
        short targetPoint = ppos.AdvanceFrom(point, distance);
        if (ppos.CarriedOff(targetPoint)) {
            return ret;
        }
        
        // Bring checker to target.
        auto& target = ret.m_points[targetPoint];
        if (target.second == opp) {
            // taking a blot
            target.first--;
            auto oppDesc = PlayerBoard::GetFor(opp);
            ret.m_points[oppDesc.Bar()].first++;
            ret.m_points[oppDesc.Bar()].second = opp;
        }
        target.first++;
        target.second = pl;
        
        ret.UpdateLegalSet();
        return ret;
    }
}
