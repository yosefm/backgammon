#pragma once

#include <set>
#include <array>
#include "bgboard.h"

namespace Purim {
    
    class LegalMovesIterator : public std::iterator<std::input_iterator_tag, short, short> {
        const PointsOccupancy m_baseline;
        Direction m_player;
        PlayerBoard m_playerDesc;
        
        value_type m_current = 0;
        short m_die = 0;
        short m_point = -1;

    public:
        LegalMovesIterator(const PointsOccupancy& baseline, Direction player, short die) 
            : m_baseline{baseline}, m_player(player), m_playerDesc{ PlayerBoard::GetFor(m_player) }, m_die{die}
        {
            if (m_die != 0) {
                operator++();
            }
        }
        
        // Iterator required interface:
        LegalMovesIterator& operator++();
        LegalMovesIterator operator++(int) {
            LegalMovesIterator retval = *this; 
            ++(*this); 
            return retval;
        }
        
        bool operator==(const LegalMovesIterator& other);
        bool operator!=(const LegalMovesIterator& other) { return !(*this == other); }
        
        // Undefined for the end iterator.
        value_type operator*() { return m_current; }
    };
    
    class LegalMovesSequence {
        const PointsOccupancy m_baseline;
        const Direction m_player;
        
        const LegalMovesIterator m_end;
        LegalMovesIterator m_begin;
        
    public:
        LegalMovesSequence(const PointsOccupancy& baseline, Direction player, short die) 
            : m_baseline{baseline}, m_player(player),
            m_end{ LegalMovesIterator(m_baseline, m_player, 0)} ,
            m_begin{ LegalMovesIterator(m_baseline, m_player, die) }  {}
        
        LegalMovesIterator& begin() { return m_begin; }
        const LegalMovesIterator& end() { return m_end; }
    };
    
    class LegalPlaysIterator : public std::iterator<std::input_iterator_tag, Play>
    {
        std::vector<short> m_diceComb;
        const Direction m_player;
        
        value_type m_current {};
        
        std::vector<LegalMovesSequence> m_levelSeqs {};
        std::vector<PointsOccupancy> m_boardStack {};
        
    public:
        LegalPlaysIterator(
            PointsOccupancy& baseline, Direction player, std::vector<short> allowedMoves)
            : m_diceComb {allowedMoves}, m_player {player}
        {
            m_boardStack.emplace_back(baseline);
            if (m_diceComb.size() > 0)
                operator++();
        }
        
        LegalPlaysIterator& operator++();
        LegalPlaysIterator operator++(int) {
            LegalPlaysIterator retval = *this; 
            ++(*this); 
            return retval;
        }
        
        bool operator==(const LegalPlaysIterator& other);
        bool operator!=(const LegalPlaysIterator& other) { return !(*this == other); }
        
        value_type operator*() { return m_current; }
    };
    
    class DecisionMaker {
    public:
        // Given a set of plays, return desired play number,
        // in set storage order. First play is #0, of course.
        virtual unsigned int SelectPlay(std::set<Play> availablePlays) = 0;
    };
    
    class HumanPlayer : public DecisionMaker {
    public:
        virtual unsigned int SelectPlay(std::set<Play> availablePlays) override;
    };
    
    class SheshBeshBot : public DecisionMaker {
        std::default_random_engine rndEng;
        
    public:
        virtual unsigned int SelectPlay(std::set<Play> availablePlays) override;
    };
}
