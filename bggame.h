#pragma once

#include "bgboard.h"
#include <set>

namespace Purim {
    class BackGammonGame {
        Direction m_nextMover = Direction::CCW;

        PointsOccupancy m_positions;
        const PointsOccupancy startPositions {
            {0, Direction::None}, // bar/ borne off
            {2, Direction::CW}, // 1
            {0, Direction::None},
            {0, Direction::None},
            {0, Direction::None},
            {0, Direction::None},
            {5, Direction::CCW}, 
            {0, Direction::None}, // 7
            {3, Direction::CCW},
            {0, Direction::None},
            {0, Direction::None},
            {0, Direction::None},
            {5, Direction::CW},
            {5, Direction::CCW}, // 13
            {0, Direction::None},
            {0, Direction::None},
            {0, Direction::None},
            {3, Direction::CW},
            {0, Direction::None},
            {5, Direction::CW}, // 19
            {0, Direction::None},
            {0, Direction::None},
            {0, Direction::None},
            {0, Direction::None},
            {2, Direction::CCW},
            {0, Direction::None} // bar/ borne off
        };
        const PointsOccupancy carryOffPositions {
            {0, Direction::None}, // bar/ borne off
            {2, Direction::CW}, // 1
            {5, Direction::CCW},
            {5, Direction::CCW},
            {5, Direction::CCW},
            {0, Direction::None},
            {0, Direction::None}, 
            {0, Direction::None}, // 7
            {0, Direction::None},
            {0, Direction::None},
            {0, Direction::None},
            {0, Direction::None},
            {5, Direction::CW},
            {0, Direction::None}, // 13
            {0, Direction::None},
            {0, Direction::None},
            {0, Direction::None},
            {3, Direction::CW},
            {0, Direction::None},
            {5, Direction::CW}, // 19
            {0, Direction::None},
            {0, Direction::None},
            {0, Direction::None},
            {0, Direction::None},
            {0, Direction::None},
            {0, Direction::None} // bar/ borne off
        };
        const PointsOccupancy blockedEntryPositions {
            {0, Direction::None}, // bar/ borne off
            {4, Direction::CCW}, // 1
            {2, Direction::CCW},
            {0, Direction::None},
            {0, Direction::None},
            {0, Direction::None},
            {1, Direction::CCW}, 
            {0, Direction::None}, // 7
            {4, Direction::CCW},
            {0, Direction::None},
            {1, Direction::CW},
            {0, Direction::None},
            {0, Direction::None},
            {0, Direction::None}, // 13
            {0, Direction::None},
            {0, Direction::None},
            {0, Direction::None},
            {1, Direction::CCW},
            {2, Direction::CW},
            {4, Direction::CW}, // 19
            {3, Direction::CW}, 
            {1, Direction::CCW},
            {2, Direction::CW}, 
            {1, Direction::CW},
            {2, Direction::CW},
            {2, Direction::CCW} // bar/ borne off
        };
        std::pair<short, short> m_dice;
        std::default_random_engine m_rndEng;
        std::uniform_int_distribution<short> m_diceRV;
    
    public:
        BackGammonGame() : m_diceRV{1, 6} { RestartGame(); }
        
        // Put checkers in starting positions.
        void RestartGame(std::vector<PointsOccupancy::Point> initialState = {});
        
        std::pair<short, short> Roll() {
            m_dice = std::make_pair(m_diceRV(m_rndEng), m_diceRV(m_rndEng));
            return m_dice;
        }
        
        std::set<Play> AvailablePlaysForNextPlayer(std::pair<short, short> roll);
        std::set<Play> AvailablePlaysForNextPlayer() { return AvailablePlaysForNextPlayer(m_dice); }
        
        enum class GameState { Ongoing, Won, Gammon, BackGammon, Error};
        
        // Perform a single play (all moves for a roll), [1]. 
        // Returns whether game ended or not (though not yet).
        GameState ExecutePlay();
        GameState ExecutePlay(Play thisPlay);
        
        // Read-only accessors:
        Direction NextPlayer() { return m_nextMover; }
        std::pair<short, short> Dice() { return m_dice; }
        const PointsOccupancy& currentState() { 
            return m_positions; 
        }
    };
}
